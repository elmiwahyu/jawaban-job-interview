import java.util.Random;
import java.util.Scanner;

class User {
    /*access modifiers (private) pada atribut-atribut kelas User yaitu:
    phoneNumber, email, fullName, password*/

    private String phoneNumber;
    private String email;
    private String fullName;
    private String password;


    public User() { // Konstruktor

    }

    public void registerAndLogin() {  //method untuk register dilanjut login
        Scanner sc = new Scanner(System.in);

        System.out.println("====================");
        System.out.println("| Pendaftaran Akun |");
        System.out.println("====================");

        System.out.println("------------------------------");
        System.out.print("Masukkan nomor HP: ");
        phoneNumber = sc.nextLine();

        System.out.print("Masukkan alamat email: ");
        email = sc.nextLine();

        System.out.print("Masukkan nama lengkap: ");
        fullName = sc.nextLine();

        System.out.print("Buat password: ");
        password = sc.nextLine();
        System.out.println("------------------------------");

        System.out.println("Akun Flip telah berhasil dibuat!");
        System.out.println("================================");
        System.out.println(" ");

        // lanjut login
        boolean loggedIn = false;
        do {
            System.out.println("=================");
            System.out.println("| Masuk ke Akun |");
            System.out.println("=================");

            System.out.println("-----------------------------");
            System.out.print("Masukkan nomor HP atau email: ");
            String usernameInput = sc.nextLine();

            System.out.print("Masukkan password: ");
            String passwordInput = sc.nextLine();
            System.out.println("-----------------------------");

            if (usernameInput.equals(phoneNumber) || usernameInput.equals(email)) {
                if (passwordInput.equals(password)) {
                    System.out.println("Selamat datang, " + fullName + "!");
                    System.out.println("==================================");
                    loggedIn = true;
                } else {
                    System.out.println("Password salah!");
                }
            } else {
                System.out.println("Nomor HP atau email tidak terdaftar!");
            }
        }while (!loggedIn);
    }


    public void login() { //method untuk langsung login
        Scanner input1 = new Scanner(System.in);
        boolean loggedIn = false;
        do {
            System.out.println("Masuk ke Akun");
            System.out.println("=============");

            System.out.print("Masukkan nomor HP atau email: ");
            String usernameInput = input1.nextLine();

            System.out.print("Masukkan password: ");
            String passwordInput = input1.nextLine();

            if (usernameInput.equals("082134637184") || usernameInput.equals("elmiwahyu@gmail.com")) {
                if (passwordInput.equals("1217")) {
                    System.out.println("Selamat datang, " + "Elmi Wahyu Triyani!");
                    loggedIn = true;
                } else {
                    System.out.println("====================================");
                    System.out.println("Password salah!");
                    System.out.println("====================================");
                }
            } else {
                System.out.println("====================================");
                System.out.println("Nomor HP atau email tidak terdaftar!");
                System.out.println("====================================");
            }
        }while (!loggedIn);
    }
}
//-------------------------------------------------------
abstract class Transaksi {
    /*penggunaan access modifiers (protected) pada atribut kelas Transaksi yaitu :
    idTransaksi, tanggal, bulan, tahun. */
    protected int idTransaksi, tanggal, bulan, tahun;


    public Transaksi(int idTransaksi, int tanggal, int bulan, int tahun) {  // konstruktor
        this.idTransaksi = idTransaksi;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public abstract void displayTransaksi();  //method abstract dideklarasikan tanpa implementasi,
}

// Class Transfer----------------------------------------------------------
class Transfer extends Transaksi {
    /* access modifiers (private) pada atribut kelas Transfer yaitu :
    bankTujuan, nomorRekening, metodetransfer, jumlahTransfer.*/
    private int bankTujuan, nomorRekening, metodetransfer;
    private int jumlahTransfer;

    public Transfer(int idTransaksi, int tanggal, int bulan, int tahun) {  //konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }

    public void transfer() { //method untuk proses transfer
        Scanner input2 = new Scanner(System.in);
        Random rand = new Random();

        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Bank Tujuan:");
        bankTujuan = input2.nextInt();

        System.out.print("Masukkan Nomor Rekening Tujuan: ");
        nomorRekening = input2.nextInt();

        System.out.print("Masukkan Jumlah Transfer (minimal 10000): ");
        jumlahTransfer = input2.nextInt();
        while (jumlahTransfer < 10000) {
            System.out.println("Jumlah transfer minimal 10000. Silahkan masukkan kembali.");
            jumlahTransfer = input2.nextInt();
        }

        jumlahTransfer += rand.nextInt(900);

        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer:");
        metodetransfer = input2.nextInt();

        System.out.println("-----------------------------------------------");

        switch (metodetransfer) {
            case 1:
                System.out.println("| Mohon Transfer ke rekening Flip: 0024011217 |");
                break;
            case 2:
                System.out.println("| Mohon Transfer ke rekening Flip: 0099101218 |");
                break;
            case 3:
                System.out.println("| Mohon Transfer ke rekening Flip: 0200081219 |");
                break;
            case 4:
                System.out.println("| Mohon Transfer ke rekening Flip: 1155041220 |");
                break;
            case 5:
                System.out.println("| Mohon Transfer ke rekening Flip: 0188021221 |");
                break;
            default:
                System.out.println("| Pilihan tidak valid. |");
                break;
        }
        System.out.println("| Nominal Transfer: " + jumlahTransfer +"           |");
        System.out.println("-----------------------------------------------");
    }


  /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi(){
        System.out.println("Memproses Transaksi Transfer");
        System.out.println("| ID Transaksi: " + idTransaksi+"|");
        System.out.println("|"+tanggal+"/"+bulan+"/"+tahun+"|");
        System.out.println("-----------------------------------------------");
    }
}

//Class Pembelian pulsa Hp--------------------------------------------------
class PulsaHp extends Transaksi {

    /* access modifiers (private) pada atribut-atribut kelas PulsaHp yaitu :
   noHP, jumlahPulsa, metodeTransfer*/
    private String nomerHp;
    private int jumlahPulsa, metodeTransfer;



    public PulsaHp(int idTransaksi, int tanggal, int bulan, int tahun) { //kontruktor untuk menginisialisasi atribut dari kelas induk
        super(idTransaksi, tanggal, bulan, tahun);
    }

    public void beliPulsa() { //method untuk proses beli pulsa
        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();


        System.out.print("Masukkan no Hp : ");
        nomerHp = input3.nextLine();
        System.out.print("Masukkan jumlah Pulsa : ");
        jumlahPulsa = input3.nextInt();

        while (jumlahPulsa < 5000) {
            System.out.println("Jumlah minimal 5000. Silahkan masukkan kembali.");
            jumlahPulsa = input3.nextInt();
        }

        jumlahPulsa += rand.nextInt(900);


        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer: ");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");

        switch (metodeTransfer) {
            case 1:
                System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                break;
            case 2:
                System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                break;
            case 3:
                System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                break;
            case 4:
                System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                break;
            case 5:
                System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
        System.out.println("Nominal Transfer: " + jumlahPulsa);
        System.out.println("-----------------------------------------------");
    }

    // implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi
    @Override
    public void displayTransaksi() {
        System.out.println("Memproses Transaksi Pembelian Pulsa");
        System.out.println("| ID Transaksi: " + idTransaksi + "|");
        System.out.println("|" + tanggal + "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }
}

//Class Pembelian paket data--------------------------------------------------------------

class paketData extends Transaksi {

    /* access modifiers (private) pada atribut kelas paketData yaitu :
   noHp, paketData, metodetransfer*/
    private String nomerHp;
    private int jumlahHarga, metodeTransfer, paketData;

    public paketData(int idTransaksi, int tanggal, int bulan, int tahun) { //Konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }

    public void belipaketData() { //method untuk proses beli paket data
        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();


        System.out.print("Masukkan no Hp : ");
        nomerHp = input3.nextLine();

        System.out.println("Pilihan paket Data:");
        System.out.println("1. 1 gb ----- 15.000");
        System.out.println("2. 3 gb ----- 25.000");
        System.out.println("3. 5 gb ----- 45.000");
        System.out.println("4. 9 gb ----- 70.000");
        System.out.println("5. 15 gb ----- 100.000");
        System.out.print(" pilih paket data: ");
        paketData = input3.nextInt();

        if (paketData == 1) {
            jumlahHarga = 15000;
        } else if (paketData == 2) {
            jumlahHarga = 25000;
        } else if (paketData == 3) {
            jumlahHarga = 45000;
        } else if (paketData == 4) {
            jumlahHarga = 70000;
        } else if (paketData == 5) {
            jumlahHarga = 100000;
        }

        jumlahHarga += rand.nextInt(900);

        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer: ");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");
        switch (metodeTransfer) {
            case 1:
                System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                break;
            case 2:
                System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                break;
            case 3:
                System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                break;
            case 4:
                System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                break;
            case 5:
                System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
        System.out.println("Nominal Transfer: " + jumlahHarga);
        System.out.println("-----------------------------------------------");
    }

    /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi() { // implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi
        System.out.println("Memproses Transaksi Pembelian Paket Data");
        System.out.println("| ID Transaksi: " + idTransaksi+ "|");
        System.out.println("|" + tanggal + "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }
}

//Class pembelian token listrik------------------------------------------------------------------------------------
class tokenListrik extends Transaksi {

    /* access modifiers (private) pada atribut kelas tokenListrik yaitu :
   nomorMeter, jumlahHarga, nominalToken, metodeTransfer.*/
    private String nomerMeter;
    private int jumlahHarga, metodeTransfer, nominalToken;

    public tokenListrik(int idTransaksi, int tanggal, int bulan, int tahun) { //Konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }

    public void belitokenListrik() { //method untuk proses beli token listrik
        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();


        System.out.print("Masukkan nomer meter/id pelanggan : ");
        nomerMeter = input3.nextLine();
        System.out.println("----------------------------------------------");

        System.out.println("-------------------------------------");
        System.out.println("|          Nominal Token:           |");
        System.out.println("-------------------------------------");
        System.out.println("| 1. 20.000 ----- harga 21.250      |");
        System.out.println("| 2. 50.000 ----- harga 51.250      |");
        System.out.println("| 3. 100.000 ----- harga 101.250    |");
        System.out.println("| 4. 200.000 ----- harga 201.250    |");
        System.out.println("| 5. 500.000 ----- harga 501.250    |");
        System.out.println("| 6. 1000.000 ----- harga 1.001.250 |");
        System.out.println("-------------------------------------");
        System.out.print("pilih: ");
        nominalToken = input3.nextInt();

        if (nominalToken == 1) {
            jumlahHarga = 21250;
        } else if (nominalToken == 2) {
            jumlahHarga = 51250;
        } else if (nominalToken == 3) {
            jumlahHarga = 101250;
        } else if (nominalToken == 4) {
            jumlahHarga = 201250;
        } else if (nominalToken == 5) {
            jumlahHarga = 501250;
        } else if (nominalToken == 6) {
            jumlahHarga = 1001250;
        }

        jumlahHarga += rand.nextInt(900);


        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer:");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");

        switch (metodeTransfer) {
            case 1:
                System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                break;
            case 2:
                System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                break;
            case 3:
                System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                break;
            case 4:
                System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                break;
            case 5:
                System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
        System.out.println("Nominal Transfer: " + jumlahHarga);
        System.out.println("-----------------------------------------------");
    }

    /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi() {
        System.out.println("Memproses Transaksi Pembelian Token Listrik");
        System.out.println("| ID Transaksi: " + idTransaksi + "|");
        System.out.println("|" + tanggal + "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }
}
//Class pembayaran tagihan listrik-------------------------------------------------------
class tagihanListrik extends Transaksi {

    /* access modifiers (private) pada atribut kelas tagihanListrik yaitu :
   idPelanggan, tagihan, metodeTransfer.*/
    private String idPelanggan;
    private int metodeTransfer, tagihan;

    public tagihanListrik(int idTransaksi, int tanggal, int bulan, int tahun) { //Konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }


    public void bayartagihanListrik() { //method untuk proses membayar tagihan listrik

        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();


        System.out.print("Masukkan nomer meter/id pelanggan : ");
        idPelanggan = input3.nextLine();
        System.out.println("----------------------------------------------");
        System.out.println("ID PELANGGAN :"+idPelanggan +("- Elmi Wahyu Triyani"));
        System.out.println("TARIF/DAYA: R1M / 900 VA");
        tagihan = 200000;
        System.out.println("RP TAG PLN : "+tagihan);
        tagihan += rand.nextInt(900);

        System.out.println("-----------------------------------------------");
        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer:");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");

        switch (metodeTransfer) {
            case 1:
                System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                break;
            case 2:
                System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                break;
            case 3:
                System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                break;
            case 4:
                System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                break;
            case 5:
                System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
        System.out.println("Nominal Transfer: " + tagihan);
        System.out.println("-----------------------------------------------");
    }

    /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi() {
        System.out.println("Memproses Transaksi Pembayaran Tagihan Listrik");
        System.out.println("| ID Transaksi: " + idTransaksi + "|");
        System.out.println("|" + tanggal+ "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }
}


//Class bayar Air PDAM------------------
class airPDAM extends Transaksi {
    /* access modifiers (private) pada atribut kelas airPDAM yaitu :
   wilayah, nomorPelanggan, tagihan, metodeTransfer.*/
    private String wilayah, nomorPelanggan;
    private int metodeTransfer, tagihan;

    public airPDAM(int idTransaksi, int tanggal, int bulan, int tahun) { //Konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }


    public void bayarPDAM() { //method untuk proses bayar air PDAM
        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();

        System.out.print("Masukan Wilayah: ");
        wilayah = input3.nextLine();
        System.out.print("Masukkan nomer meter/id pelanggan : ");
        nomorPelanggan = input3.nextLine();
        System.out.println("----------------------------------------------");
        System.out.println("ID PELANGGAN :"+nomorPelanggan +("- Elmi Wahyu Triyani"));
        tagihan = 500000;
        System.out.println("RP TAG PLN : "+tagihan);
        tagihan += rand.nextInt(900);
        System.out.println("-----------------------------------------------");

        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer:");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");

        switch (metodeTransfer) {
            case 1:
                System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                break;
            case 2:
                System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                break;
            case 3:
                System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                break;
            case 4:
                System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                break;
            case 5:
                System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
        System.out.println("Nominal Transfer: " + tagihan);
        System.out.println("-----------------------------------------------");
    }

    /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi() {
        System.out.println("Memproses Transaksi Pembayaran Air PDAM");
        System.out.println("| ID Transaksi: " + idTransaksi + "|");
        System.out.println("|" + tanggal+ "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }
}
//Class E Wallet---------------
class Ewallet extends Transaksi {

    /* access modifiers (private) pada atribut kelas Ewallet yaitu :
   jeniEwalet, nomorHp, jumlahTopUp, metodeTransfer.*/
    private String jenisEWalet;
    private String nomorHp;
    private int jumlahTopUp;
    private int metodeTransfer;

    public Ewallet(int idTransaksi, int tanggal, int bulan, int tahun) { //Konstruktor
        super(idTransaksi, tanggal, bulan, tahun);
    }
    public void tampilInfoTopUp() { //method untuk menampilkan detail Top Up
        System.out.println("Detail Transaksi:");
        System.out.println("Jenis e-wallet: " + jenisEWalet);
        System.out.println("Nomor HP: " + nomorHp);
        System.out.println("Jumlah top-up: " + jumlahTopUp);
        System.out.println("-----------------------------------------------");
    }
    public void topupEwallet() { //method untuk proses top up e wallet
        Scanner input3 = new Scanner(System.in);
        Random rand = new Random();

        // Tampilkan pilihan e-wallet
        System.out.println("---------------");
        System.out.println("1. Dana");
        System.out.println("2. Gopay");
        System.out.println("3. LinkAja");
        System.out.println("4. OVO");
        System.out.println("5. Shopeepay");
        System.out.println("---------------");
        System.out.print("Pilih E-Wallet Tujuan: ");
        int pilihanEWalet = input3.nextInt();
        input3.nextLine();
        switch (pilihanEWalet) {
            case 1:
                jenisEWalet = "Dana";
                break;
            case 2:
                jenisEWalet = "Gopay";
                break;
            case 3:
                jenisEWalet = "LinkAja";
                break;
            case 4:
                jenisEWalet = "OVO";
                break;
            case 5:
                jenisEWalet = "Shopeepay";
                break;
            default:
                jenisEWalet = "Tidak valid";
                break;
        }

        System.out.print("Masukkan nomor E-Wallet: ");
        nomorHp = input3.nextLine();

        // Minta pengguna memasukkan jumlah top-up
        System.out.print("Jumlah Top Up: ");
        jumlahTopUp = input3.nextInt();

        jumlahTopUp += rand.nextInt(900);
        System.out.println("-----------------------------------------------");

        System.out.println("--------------");
        System.out.println("| 1. BRI     |");
        System.out.println("| 2. BNI     |");
        System.out.println("| 3. BSI     |");
        System.out.println("| 4. Mandiri |");
        System.out.println("| 5. BCA     |");
        System.out.println("--------------");
        System.out.print("Pilih Metode Transfer:");
        metodeTransfer = input3.nextInt();
        System.out.println("-----------------------------------------------");

            switch (metodeTransfer) {
                case 1:
                    System.out.println("Mohon Transfer ke rekening Flip: 0024011217");
                    break;
                case 2:
                    System.out.println("Mohon Transfer ke rekening Flip: 0099101218");
                    break;
                case 3:
                    System.out.println("Mohon Transfer ke rekening Flip: 0200081219");
                    break;
                case 4:
                    System.out.println("Mohon Transfer ke rekening Flip: 1155041220");
                    break;
                case 5:
                    System.out.println("Mohon Transfer ke rekening Flip: 0188021221");
                    break;
                default:
                    System.out.println("Pilihan tidak valid.");
                    break;
            }
            System.out.println("Nominal Transfer: " + jumlahTopUp);
            System.out.println("-----------------------------------------------");
        }

    /*implementasi dari metode abstrak displayTransaksi() yang didefinisikan di kelas induk Transaksi*/
    @Override
    public void displayTransaksi() {
        System.out.println("Memproses Transaksi Top Up E-Wallet");
        System.out.println("| ID Transaksi: " + idTransaksi + "|");
        System.out.println("|" + tanggal + "/" + bulan + "/" + tahun + "|");
        System.out.println("-----------------------------------------------");
    }

    }

    //-------------------------------------------------------------------------
    public class Main {
        public static void main(String[] args) {
            User user = new User();  //pembuatan objek dari kelas User
            Scanner sc = new Scanner(System.in);

            System.out.println("=======================");
            System.out.println("| Ayo gabung ke Flip! |");
            System.out.println("=======================");
            System.out.println(" ");

            System.out.println("-------------");
            System.out.println("| 1. Daftar |");
            System.out.println("-------------");
            System.out.println("-------------");
            System.out.println("| 2. Masuk |");
            System.out.println("-------------");
            System.out.print("Pilih: ");
            int choice = sc.nextInt();
            System.out.println("========================");

            switch (choice) {
                case 1:
                    user.registerAndLogin();
                    int choice2;
                    do {
                        System.out.println("----------------------");
                        System.out.println("| 1. Transfer        |");
                        System.out.println("| 2. Pulsa (Hp)      |");
                        System.out.println("| 3. Paket Data      |");
                        System.out.println("| 4. Token Listrik   |");
                        System.out.println("| 5. Tagihan Listrik |");
                        System.out.println("| 6. Air PDAM        |");
                        System.out.println("| 7. E- Wallet       |");
                        System.out.println("----------------------");
                        System.out.print("Pilih : ");
                        Scanner in = new Scanner(System.in);
                        choice2 = in.nextInt();
                        System.out.println("=====================");
                        in.nextLine();
                        switch (choice2) {
                            case 1:
                                Transfer transfer = new Transfer(1, 17, 5, 2023); //membuat objek transfer dari kelas Transfer
                                transfer.transfer(); // pemanggilan objek dan method
                                transfer.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 2:
                                PulsaHp pulsa = new PulsaHp(2, 17, 5, 2023); //membuat objek pulsa dari kelas pulsaHp
                                pulsa.beliPulsa(); // pemanggilan objek dan method
                                pulsa.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 3:
                                paketData paketData1 = new paketData(3, 17, 5, 2023); //membuat objek paketData1 dari kelas paketData
                                paketData1.belipaketData();// pemanggilan objek dan method
                                paketData1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 4:
                                tokenListrik tokenListrik1 = new tokenListrik(4, 17, 5, 2023); //membuat objek tokenListrik1 dari kelas tokenListrik
                                tokenListrik1.belitokenListrik();// pemanggilan objek dan method
                                tokenListrik1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 5:
                                tagihanListrik tagihanListrik1 = new tagihanListrik(5, 17, 5, 2023); //membuat objek tagihanListrik1 dari kelas tagihanListrik
                                tagihanListrik1.bayartagihanListrik();// pemanggilan objek dan method
                                tagihanListrik1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 6:
                                airPDAM airPDAM1 = new airPDAM(6, 17, 5, 2023); //membuat objek airPDAM1 dari kelas airPDAM
                                airPDAM1.bayarPDAM();// pemanggilan objek dan method
                                airPDAM1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 7:
                                Ewallet Ewallet1 = new Ewallet(7, 17,5,2023); //membuat objek Ewallet1 dari kelas Ewallet
                                Ewallet1.topupEwallet();// pemanggilan objek dan method
                                Ewallet1.tampilInfoTopUp();// pemanggilan objek dan method
                                Ewallet1.displayTransaksi();// pemanggilan objek dan method


                        }

                        System.out.print("Apakah Anda ingin kembali ke menu utama? (1/0) : ");
                        choice2 = in.nextInt();
                        in.nextLine();

                    } while (choice2 == 1);


                    break;
                case 2:
                    user.login();
                    do {
                        System.out.println("----------------------");
                        System.out.println("| 1. Transfer        |");
                        System.out.println("| 2. Pulsa (Hp)      |");
                        System.out.println("| 3. Paket Data      |");
                        System.out.println("| 4. Token Listrik   |");
                        System.out.println("| 5. Tagihan Listrik |");
                        System.out.println("| 6. Air PDAM        |");
                        System.out.println("| 7. E- Wallet       |");
                        System.out.println("----------------------");
                        System.out.print("Pilih : ");
                        Scanner in = new Scanner(System.in);
                        choice2 = in.nextInt();
                        System.out.println("=====================");
                        in.nextLine();
                        switch (choice2) {
                            case 1:
                                Transfer transfer = new Transfer(1, 17, 5, 2023); //membuat objek transfer dari kelas Transfer
                                transfer.transfer();// pemanggilan objek dan method
                                transfer.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 2:
                                PulsaHp pulsa = new PulsaHp(2, 17, 5, 2023); //membuat objek pulsa dari kelas pulsaHp
                                pulsa.beliPulsa();// pemanggilan objek dan method
                                pulsa.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 3:
                                paketData paketData1 = new paketData(17, 9, 5, 2023);//membuat objek paketData1 dari kelas paketData
                                paketData1.belipaketData();
                                paketData1.displayTransaksi();
                                break;
                            case 4:
                                tokenListrik tokenListrik1 = new tokenListrik(4, 17, 5, 2023); //membuat objek tokenListrik1 dari kelas tokenListrik
                                tokenListrik1.belitokenListrik();// pemanggilan objek dan method
                                tokenListrik1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 5:
                                tagihanListrik tagihanListrik1 = new tagihanListrik(5, 17, 5, 2023); //membuat objek tagihanListrik1 dari kelas tagihanListrik
                                tagihanListrik1.bayartagihanListrik();// pemanggilan objek dan method
                                tagihanListrik1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 6:
                                airPDAM airPDAM1 = new airPDAM(6, 17, 5, 2023); //membuat objek airPDAM1 dari kelas airPDAM
                                airPDAM1.bayarPDAM();// pemanggilan objek dan method
                                airPDAM1.displayTransaksi();// pemanggilan objek dan method
                                break;
                            case 7:
                                Ewallet Ewallet1 = new Ewallet(7, 17,5,2023); //membuat objek Ewallet1 dari kelas Ewallet
                                Ewallet1.topupEwallet();// pemanggilan objek dan method
                                Ewallet1.tampilInfoTopUp();// pemanggilan objek dan method
                                Ewallet1.displayTransaksi();// pemanggilan objek dan method


                        }

                        System.out.print("Apakah Anda ingin kembali ke menu utama? (1/0) : ");
                        choice2 = in.nextInt();
                        in.nextLine();

                    } while (choice2 == 1);
                    break;
                default:
                    System.out.println("Menu tidak tersedia!");
                    break;
            }
        }
    }

