# no.1
Pada program saya menggunakan struktur kontrol yaitu  pernyataan pengulangan (loop) dan percabangan (branching). Algoritma yang digunakan dalam program tersebut adalah algoritma pemrosesan transaksi berdasarkan pilihan user.
Program dimulai dengan menampilkan menu daftar atau masuk. Pilihan user kemudian dibaca menggunakan Scanner. Berdasarkan pilihan tersebut, program akan memanggil metode registerAndLogin() jika user memilih daftar atau login() jika user memilih masuk. Setelah user berhasil mendaftar atau masuk, program akan menampilkan menu transaksi yang berisi beberapa opsi seperti transfer, pembelian pulsa, paket data, token listrik, tagihan listrik, air PDAM, dan E-Wallet. Pilihan user kemudian dibaca lagi menggunakan Scanner. Berdasarkan pilihan user, program akan memanggil metode yang sesuai untuk memproses transaksi yang dipilih, misalnya transfer.transfer() untuk transfer, pulsa.beliPulsa() untuk pembelian pulsa, dan seterusnya. Setelah transaksi selesai diproses, program akan menampilkan informasi transaksi menggunakan metode displayTransaksi(). Setelah itu, user akan ditanyai apakah ingin kembali ke menu utama. Jika user memilih untuk kembali, program akan kembali ke menu utama. Jika tidak, program akan berakhir.
Algoritma tersebut memanfaatkan percabangan dengan menggunakan switch-case untuk memilih aksi berdasarkan pilihan user, dan pengulangan dengan menggunakan do-while untuk user pengulangan melakukan beberapa transaksi sesuai keinginan user .


Berikut adalah Pseudocode program main yang saya buat:
    
    class Main:
    main():
        user = User()
        sc = Scanner(System.in)

        print("=======================")
        print("| Ayo gabung ke Flip! |")
        print("=======================")
        print(" ")

        print("-------------")
        print("| 1. Daftar |")
        print("-------------")
        print("-------------")
        print("| 2. Masuk  |")
        print("-------------")
        print("Pilih: ")
        choice = sc.nextInt()
        print("========================")

        switch (choice):
            case 1:
                user.registerAndLogin()
                choice2 = 0
                while (choice2 == 1):
                    print("----------------------")
                    print("| 1. Transfer        |")
                    print("| 2. Pulsa (Hp)      |")
                    print("| 3. Paket Data      |")
                    print("| 4. Token Listrik   |")
                    print("| 5. Tagihan Listrik |")
                    print("| 6. Air PDAM        |")
                    print("| 7. E-Wallet        |")
                    print("----------------------")
                    print("Pilih: ")
                    choice2 = sc.nextInt()
                    print("=====================")
                    sc.nextLine()

                    switch (choice2):
                        case 1:
                            transfer = Transfer(1, 9, 5, 2023)
                            transfer.transfer()
                            transfer.displayTransaksi()
                            break
                        case 2:
                            pulsa = PulsaHp(2, 9, 5, 2023)
                            pulsa.beliPulsa()
                            pulsa.displayTransaksi()
                            break
                        case 3:
                            paketData1 = PaketData(3, 9, 5, 2023)
                            paketData1.belipaketData()
                            paketData1.displayTransaksi()
                            break
                        case 4:
                            tokenListrik1 = TokenListrik(4, 9, 5, 2023)
                            tokenListrik1.belitokenListrik()
                            tokenListrik1.displayTransaksi()
                            break
                        case 5:
                            tagihanListrik1 = TagihanListrik(5, 9, 5, 2023)
                            tagihanListrik1.bayartagihanListrik()
                            tagihanListrik1.displayTransaksi()
                            break
                        case 6:
                            airPDAM1 = AirPDAM(6, 9, 5, 2023)
                            airPDAM1.bayarPDAM()
                            airPDAM1.displayTransaksi()
                            break
                        case 7:
                            ewallet1 = Ewallet(7, 9, 5, 2023)
                            ewallet1.topupEwallet()
                            ewallet1.tampilInfoTopUp()
                            ewallet1.displayTransaksi()

                    print("Apakah Anda ingin kembali ke menu utama? (1/0) : ")
                    choice2 = sc.nextInt()
                    sc.nextLine()

            case 2:
                user.login()
                choice2 = 0
                while (choice2 == 1):
                    print("----------------------")
                    print("| 1. Transfer        |")
                    print("| 2. Pulsa (Hp)      |")
                    print("| 3. Paket Data      |")
                    print("| 4. Token Listrik   |")
                    print("| 5. Tagihan Listrik |")
                    print("| 6. Air PDAM        |")
                    print("| 7. E-Wallet        |")
                    print("----------------------")
                    print("Pilih: ")
                    choice2 = sc.nextInt()
                    print("=====================")
                    sc.nextLine()
                switch (choice2):
                    case 1:
                        transfer = Transfer(1, 9, 5, 2023)
                        transfer.transfer()
                        transfer.displayTransaksi()
                        break
                    case 2:
                        pulsa = PulsaHp(2, 9, 5, 2023)
                        pulsa.beliPulsa()
                        pulsa.displayTransaksi()
                        break
                    case 3:
                        paketData1 = PaketData(3, 9, 5, 2023)
                        paketData1.belipaketData()
                        paketData1.displayTransaksi()
                        break
                    case 4:
                        tokenListrik1 = TokenListrik(4, 9, 5, 2023)
                        tokenListrik1.belitokenListrik()
                        tokenListrik1.displayTransaksi()
                        break
                    case 5:
                        tagihanListrik1 = TagihanListrik(5, 9, 5, 2023)
                        tagihanListrik1.bayartagihanListrik()
                        tagihanListrik1.displayTransaksi()
                        break
                    case 6:
                        airPDAM1 = AirPDAM(6, 9, 5, 2023)
                        airPDAM1.bayarPDAM()
                        airPDAM1.displayTransaksi()
                        break
                    case 7:
                        ewallet1 = Ewallet(7, 9, 5, 2023)
                        ewallet1.topupEwallet()
                        ewallet1.tampilInfoTopUp()
                        ewallet1.displayTransaksi()

                print("Apakah Anda ingin kembali ke menu utama? (1/0) : ")
                choice2 = sc.nextInt()
                sc.nextLine()

        default:
            print("Menu tidak tersedia!")
            break


![Dokumentasi_Flip](Dokumentasi_Flip/Algoritma_Deskriptif.jpg)


# no.2
Saya memilih aplikasi flip untuk mengerjakan tugas ini. Flip bekerja sebagai jembatan transaksi antar bank dimana pengguna cukup melakukan transfer terlebih dahulu ke rekening Flip yang sama dengan bank yang dimiliki pengguna, kemudian flip akan meneruskan transaksi ke rekening tujuan, dengan ini pengguna dapat menghemat biaya admin. Ada beberapa menu dalam aplikasi flip namun yang sudah saya garap adalah menu Tranfer, beli Pulsa, paket data, token listrik, tagihan Listrik, bayar Air PDAM, dan top Up E-wallet. Gambaran umum pada program yang saya buat adalah pada class user terdapat atribut phoneNumber, email, fullname, dan password pada class ini user diminta untuk memilih daftar atau masuk. Apabila user memilih daftar maka method registerAndlogin() dijalankan, sementara apabila user memilih masuk maka method login() akan dijalankan. Terdapat kelas Transaksi dimana kelas transaksi ini dijadikan parent class karena terdapat atribut yang diwarisi child class(child class pada program saya adalah menu menu pada aplikasi flip).
Berikut adalah link untuk melihat source code program: [source code flip](Main.java)

                
# no.3

1.	Abstraction
    
    Abstraction adalah salah satu pilar oop yang memungkinkan untuk menyembunyikan detail implementasi/bentuknya masih abstract/menggambarkan karakteristik umum. Class abstract dapat memiliki method abstract yang nantinya bentuk implementasi method abstract nya di gunakan di kelas turunan. Jadi Class abstact digunakan ketika kita ingin membuat kerangka dasar untuk kelas-kelas turunan yang berbagi karakteristik atau perilaku tertentu. Jadi keuntungan menggunakan Abstraction adalaha Ketika kita ingin membuat program yang memberikan implementasi dasar untuk beberapa metode tetapi tetap memungkinkan kelas-kelas turunan untuk memperluas atau menyesuaikan perilaku tersebut. 
2.	Enkapsulasi
    
    Dengan menggunakan enkapsulasi maka kita dapat membatasi/mengatur hak akses pada atribut/method sehingga mencegah perubahan data(keamanan).Sehingga manfaat dari Enkapsulasi adalah sebagai data protection. Terdapat 3 accses modifier yaitu public, protected, dan privat. Jika method dan atribut menggunakan access modifier public maka method dan atribut tersebut dapat diakses didalam/diluar kelas. Jika method dan atribut menggunakan access modifier protected maka method dan atribut tidak bisa diakses di luar kelas tetapi bisa diakses dikelas itu sendiri. Jika method dan atribut menggunakan access modifier private maka method dan atribut tersebut hanya dapat diakses didalam kelas, private tidak dapat diakses langsung dari luar kelas dan biasanya diakses melalui method publik (geter dan setter) di dalam kelas.
3.	Polymorphism
    
    Polymorphism merupakan keadaan suatu objek dimana objek dapat memiliki banyak bentuk yang berbeda(berbeda-beda tapi namanya sama). Terdapat dua macam Polymorphism yaitu:   
    
    1.	Polymorphism statis menggunakan method overloading.
Method overloading terjadi pada sebuah class yang memiliki nama method yang sama tapi memiliki parameter dan tipe data yang berbeda.
    2.	Polymorphism dinamis menggunakan method overriding.
Biasanya digunakan saat inheritance,  kita bisa mewariskan atribut dan method dari class induk ke class anak. Class anak akan memiliki nama method yang sama dengan class induknya tapi nanti isi dan parameternya bisa berbeda dari class induk
4.	Inheritance
    
    Inheritance adalah konsep oop dimana sebuah kelas(child) dapat mewarisi sifat dari kelas lain(parent). Keuntungan menggunakan inheritance adalah keefektifan dalam proses koding, karena tidak perlu menulis berulang-ulang properti dan method yang sama.

# no.4 
Berikut uraian implementasi pada kode program yang saya buat:
•	Class User : enkapsulasi diimplementasikan pada penggunaan access modifiers (private) pada atribut-atribut kelas User yaitu: phoneNumber, email, fullName, password. Dengan menerapkan access modifiers private pada atribut tersebut, akses langsung dari luar kelas ke atribut-atribut tersebut akan dibatasi.
•	Class Transaksi : enkapsulasi diimplementasikan pada penggunaan access modifiers (protected) pada atribut-atribut kelas Transaksi yaitu : idTransaksi, tanggal, bulan, tahun. Access modifiers protected digunakan karena  pada atribut pada kelas ini supaya  tetap dapat diubah oleh kelas-kelas turunan yang meng-extend Transaksi. 
•	Class Transfer : enkapsulasi diimplementasikan pada penggunaan access modifiers (private) pada atribut-atribut kelas yaitu : bankTujuan, nomorRekening, metodetransfer, jumlahTransfer. Atribut-atribut tersebut diberi access modifier private, yang berarti hanya dapat diakses dan diubah nilainya melalui metode-metode yang ada di kelas Transfer sendiri. 
Dan pada class lainnya dapat dilihat pada source code lengkapnya : [source code flip](Main.java) yang sudah diberi komentar


# no. 5
Kelas abstract pada program saya adalah Transaksi yang memiliki beberapa atribut (idTransaksi, tanggal, bulan, tahun). Kelas ini juga memiliki sebuah method abstrak displayTransaksi(), yang belum diimplementasikan. Method displayTransaksi() diimplementasikan untuk menampilkan informasi transaksi. Method displayTransaksi() dimplemtasi pada kelas turunan(menu-menu pada aplikasi flip) yaitu pada Class Transfer, pulsaHp, paketData, tokenListrik, tagihanListrik, airPDAM, dan EWallet.

    
    abstract class Transaksi {
    /*penggunaan access modifiers (protected) pada atribut kelas Transaksi yaitu :
    idTransaksi, tanggal, bulan, tahun. */
    protected int idTransaksi, tanggal, bulan, tahun;


    public Transaksi(int idTransaksi, int tanggal, int bulan, int tahun) {  // konstruktor
        this.idTransaksi = idTransaksi;
        this.tanggal = tanggal;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public abstract void displayTransaksi();  //method abstract dideklarasikan tanpa implementasi,
    }   



# no.6
1.	Inheritance (Pewarisan)
Class Transfer, pulsaHp, paketData, tokenListrik, tagihanListrik, airPDAM, dan EWallet merupakan subclass(child class) dari kelas Transaksi(parent class), yang ditunjukkan dengan keyword extends. Dengan inheritance, child class  mewarisi semua atribut dan metode yang didefinisikan dalam kelas Transaksi(parent class). Dalam konstruktor pada childclass, keyword super() digunakan untuk memanggil konstruktor superclass (Transaksi) dan menginisialisasi atribut yang diwarisi.

2.	Polymorphism
Method displayTransaksi() pada kelas Transaksi dideklarasikan sebagai methode abstract, yang berarti tidak memiliki implementasi di kelas TransaksiI ni menggambarkan polimorfisme, di mana kelas turunan (Class Transfer, pulsaHp, paketData, tokenListrik, tagihanListrik, airPDAM, dan EWallet) memberikan implementasi berbeda bentuk untuk metode yang diwarisi dari parent class (Transaksi). Pada child class, terdapat notasi @Override sebelum deklarasi metode displayTransaksi(), yang menunjukkan bahwa metode tersebut merupakan overriding dari metode yang sama di superclass. Dalam hal ini, metode displayTransaksi() diimplementasikan untuk menampilkan informasi transaksi transfer yang spesifik untuk child class. 

Beriku kode lengkapnya : [source code flip](Main.java)

   
            

# no.7

Dilakukan dengan Identifikasi objek-objek yang terlibat dalam proses bisnis. Pada program saya melibatkan menu menu pada aplikasi flip maka setiap menu saya jadikan kelas-kelas dalam OOP.

Pada class user terdapat atribut phoneNumber, email, fullname, dan password pada class ini user diminta untuk memilih daftar. Apabila user memilih daftar maka method registerAndlogin() dijalankan, sementara apabila user memilih masuk maka method login() akan dijalankan.
Terdapat kelas Transaksi dimana kelas transaksi ini dijadikan parent class karena terdapat atribut yang diwarisi child class(child class pada program saya adala menu menu pada aplikasi flip). Dilanjutkan dengan user dapat memilih jenis transaksi. 
1.	Class Transfer terdapat atribut bank tujuan, nomor Rekening, metodeTransfer, jumlahTransfer dan method Transfer(). 
2.	Class pulsaHp terdapat atribut nomerHp, jumlahPulsa, metodeTransfer, dan method yang digunakan untuk memproses pembelian pulsa adalah beliPulsa(). 
3.	Class paketData memiliki atribut nomerHp, paketData, jumlahHarga,dan metodeTransfer, method yang digunakan adalah belipaketData().
4.	Class tokenListrik memiliki atribut nomerMeter, nominalToken, jumlahHarga, metodeTransfer, dan method yang digunakan untuk memproses pembelian token listrik adalah beliTokenListrik().
5.	Class tagihanListrik memiliki atribut idPelanggan, tagihan, metodeTransfer, dan method bayartagihanListrik().
6.	Class airPDAM memiliki atribut wilayah, nomerPelanggan, tagihan, metodeTranfer dan method yang digunakan adalah bayarPDAM().
7.	Class EWallet memiliki atribut jenisEwalet, nomerHp, jumlahTopUp, metodeTransfer dan menggunakan method tampilinfoTopUp() untuk menampilkan detail transaksi dan method topUpEwallet() untuk memproses transaksi.

# no.8
Berikut adalah link untuk melihat use case table: [Use_Case](Use_Case_dan_Class_Diagram.pdf)
Diagram class : [digram_class](Dokumentasi_Flip/Class_diagram.png)

Pada class diagram ini user dapat melakukan beberapa transaksi. Pada class user terdapat atribut phoneNumber, email, fullname, dan password pada class ini user diminta untuk memilih daftar atau masuk. Apabila user memilih daftar maka method registerAndlogin() dijalankan, sementara apabila user memilih masuk maka method login() akan dijalankan.
Terdapat kelas Transaksi dimana kelas transaksi ini dijadikan parent class karena terdapat atribut yang mencakup atribut child class(child class pada program saya adala menu menu pada aplikasi flip). Dilanjutkan dengan user dapat memilih jenis transaksi masing-masing transaksi memiliki atribut seperti yang tercantum pada class diagram diatas. Pada atribut metode pembayaran disini adalah bukan method karena atribut ini digunakan ketika user memilih jenis bank untuk metode pembayaran. Masing-masing menu memanggil method untuk melakukan proses transaksi seperti Tranfer(), beliPulsa(), belipaketData, belitokenListrik(), bayartagihanListrik(), bayarPDAM, dan topUpEwalet().

# no.9
 link video youtube: [Youtube](https://youtu.be/Q9RsLIKgGw4)
 
# no.10
link video youtube: [Youtube](https://youtu.be/Q9RsLIKgGw4)
